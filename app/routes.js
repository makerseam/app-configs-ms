const express = require('express');
const EventsController = require('./controllers/EventsController');

const router = express.Router();

// Orders
// Create order
router.get('/configs', EventsController.get);

module.exports = router;
