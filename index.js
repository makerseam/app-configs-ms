const express = require('express');
require('dotenv').config();
const bodyParser = require('body-parser');
const routes = require('./app/routes');
const cors = require('cors');
const { ErrorHandler } = require('./app/utils/ErrorHandlerMiddleware');
const log4js = require('log4js');
const { PREFIX } = require('./app/config/AppConfig');

console.log(`ENVIMOREMENTS: ${JSON.stringify(process.env)}`);

const app = express();
const { PORT = 3000 } = process.env;

app.use(cors({
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  preflightContinue: false,
  optionsSuccessStatus: 204,
}));

app.use(bodyParser.json());

const logger = log4js.getLogger('devices-ms');
process.on('unhandledRejection', (reason, p) => {
  logger.error('Unhandled Rejection at: Promise', p, 'reason:', reason);
  logger.error(reason.stack);
});

app.use(PREFIX, routes);
app.use(ErrorHandler);

app.listen(PORT, () => {
  console.log('Escuchando puerto:', PORT);
});

module.exports = app;
