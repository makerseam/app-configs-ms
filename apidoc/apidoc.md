Orders MS

## Version: 1.0.0

### /orders

#### POST
##### Description:

Register a order with products, user address and user order

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | Order object | Yes | [OrderCreate](#ordercreate) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK and json with data of registers |
| 400 | Bad request. Error in params |
| 401 | Authorization information is missing or invalid. |
| 404 | Entity not found. |
| 5XX | Unexpected error. |

### /users/{applicationUserId}/actives

#### GET
##### Description:

get orders actives of user

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| applicationUserId | path | ID of user to get | Yes | long |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK and json with data of registers |
| 400 | Bad request. Error in params |
| 401 | Authorization information is missing or invalid. |
| 404 | Entity not found. |
| 5XX | Unexpected error. |

### /orders/{orderId}/assign-order

#### PUT
##### Description:

assign shopper_id to order

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| orderId | path | Id of order to cancel | Yes | long |
| body | body | Order to edit its shopper_id | Yes | [OrderAssign](#orderassign) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK and json with data of registers |
| 400 | Bad request. Error in params |
| 401 | Authorization information is missing or invalid. |
| 404 | Entity not found. |
| 5XX | Unexpected error. |

### /orders/{orderId}/cancel-order

#### PUT
##### Description:

cancel order

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| orderId | path | Id of order to cancel | Yes | long |
| body | body | body of request of order | Yes | [OrderCancel](#ordercancel) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK and json with data of registers |
| 400 | Bad request. Error in params |
| 401 | Authorization information is missing or invalid. |
| 404 | Entity not found. |
| 5XX | Unexpected error. |

### /stores/{storeId}

#### GET
##### Description:

get store orders by state

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| storeId | path | store Id to get | Yes |  |
| state | query | state to search of the order | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK |
| 400 | Bad request. Error in params |
| 401 | Authorization information is missing or invalid. |
| 404 | Entity not found. |
| 5XX | Unexpected error. |

### /orders/{orderId}

#### GET
##### Description:

get orders by id

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| orderId | path | order Id to get | Yes |  |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK |
| 400 | Bad request. Error in params |
| 401 | Authorization information is missing or invalid. |
| 404 | Entity not found. |
| 5XX | Unexpected error. |

### /shoppers/{shopperId}/assigned-orders

#### GET
##### Description:

get shopper orders by state assigned

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| shopperId | path | shopper Id to get | Yes | integer |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK |
| 400 | Bad request. Error in params |
| 401 | Authorization information is missing or invalid. |
| 404 | Entity not found. |
| 5XX | Unexpected error. |

### /orders/{orderId}/finish-order

#### PUT
##### Description:

finish order

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| orderId | path | Id of order to finish | Yes | integer |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK and json with data of registers |
| 400 | Bad request. Error in params |
| 401 | Authorization information is missing or invalid. |
| 404 | Entity not found. |
| 5XX | Unexpected error. |

### /orders/{orderId}/remove-product

#### PUT
##### Description:

remove product to order

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| orderId | path | Id of order to remove product | Yes | long |
| body | body | body of request of order | Yes | [OrderRemoveProduct](#orderremoveproduct) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK and json with data of registers |
| 400 | Bad request. Error in params |
| 401 | Authorization information is missing or invalid. |
| 404 | Entity not found. |
| 5XX | Unexpected error. |

### /orders/{orderId}/add-product

#### PUT
##### Description:

add product to an order

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| orderId | path | Id of order to add product | Yes | integer |
| body | body | body of request of order | Yes | [OrderAddProduct](#orderaddproduct) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK and json with data of registers |
| 400 | Bad request. Error in params |
| 401 | Authorization information is missing or invalid. |
| 404 | Entity not found. |
| 5XX | Unexpected error. |

### /orders/{orderId}/replace-product

#### PUT
##### Description:

replace product to an order

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| orderId | path | Id of order to replace product | Yes | integer |
| body | body | body of request of order | Yes | [OrderReplaceProduct](#orderreplaceproduct) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK and json with data of registers |
| 400 | Bad request. Error in params |
| 401 | Authorization information is missing or invalid. |
| 404 | Entity not found. |
| 5XX | Unexpected error. |

### /orders/{orderId}/order-products/{orderProductId}

#### PUT
##### Description:

update quantity product to an order

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| orderId | path | Id of order to update quantity product | Yes | integer |
| orderProductId | path | Id of order_product to update quantity | Yes | integer |
| body | body | body of request of order | Yes | [OrderUpdateQuantityProduct](#orderupdatequantityproduct) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK and json with data of registers |
| 400 | Bad request. Error in params |
| 401 | Authorization information is missing or invalid. |
| 404 | Entity not found. |
| 5XX | Unexpected error. |

### /orders/{orderId}/start-order

#### PUT
##### Description:

start an order

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| orderId | path | Id of order to start | Yes | integer |
| body | body | body of request of order | Yes | [OrderStart](#orderstart) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK and json with data of registers |
| 400 | Bad request. Error in params |
| 401 | Authorization information is missing or invalid. |
| 404 | Entity not found. |
| 5XX | Unexpected error. |
