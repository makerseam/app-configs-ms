const EventsController = module.exports;
const log4j = require('../utils/logger');
const LogUtils = require('../utils/LogUtils');


EventsController.get = (req, res) => {
  const logName = 'Create Order';
  const logger = LogUtils.getLoggerWithId(log4j, logName);

  logger.info('Starting EventsController.get');

  const {
    FIREBASE_APIKEY: firebasekey,
    FIREBASE_AUTHDOMAIN: firebasedomain,
    FIREBASE_DATABASE_URL: firebasedburl,
    FIREBASE_PROJECTID: firebaseproject,
  } = process.env;

  return res.send({
    FIREBASE_APIKEY: firebasekey,
    FIREBASE_AUTHDOMAIN: firebasedomain,
    FIREBASE_DATABASE_URL: firebasedburl,
    FIREBASE_PROJECTID: firebaseproject,
  });
};

