const EventsService = module.exports;
const EventsRepository = require('../repositories/EventsRepository');

EventsService.saveEvents = (topic, message, options = {}) => {
  const { logger = console } = options;

  logger.info(`Arriving message ${JSON.stringify({ topic, message })}`);

  return EventsRepository.create({ topic, message }).catch(error => console.log(error));
};
