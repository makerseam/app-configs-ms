const KafkaClient = module.exports;
const { Kafka } = require('kafkajs');
const { KafkaJSNumberOfRetriesExceeded } = require('kafkajs/src/errors');

const moment = require('moment-timezone');


function createKafka(config) {
  const {
    host, groupId, api_key: username, secret: password,
  } = config;
  const kafka = new Kafka({
    clientId: `${groupId} + ${moment()}`,
    brokers: [host],
    ssl: true,
    sasl: {
      mechanism: 'plain',
      username,
      password,
    },
  });

  return kafka;
}

KafkaClient.createConsumer = async (config, options = {}) => {
  const { logger = console } = options;
  const { groupId, handler, topic: topicToConsume } = config;
  const kafka = createKafka(config);

  const consumer = kafka.consumer({ groupId });
  logger.debug(`Consumer created ${JSON.stringify(config)}`);

  await consumer.connect();
  await consumer.subscribe({ topic: topicToConsume, fromBeginning: false });
  logger.debug(`Consumer subscribed ${JSON.stringify(config)}`);

  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      const { key, value, offset } = message;

      logger.debug('Kafka message', `partition:${partition}`, `topic:${topic}`, `offset:${offset}`, `key:${key}`);

      try {
        const data = JSON.parse(value);
        logger.debug('Kafka Payload: info: ', data);

        await handler(data, { logger: this.logger, logName: this.loggerName }, topic, partition, offset, key, value);
      } catch (error) {
        logger.debug('Error parsing kafka event to JSON', error.message);
      }
    },
  });

  return kafka;
};


class Producer {
  constructor(config, options = {}) {
    const { logger = console } = options;
    const kafka = createKafka(config);
    this.logger = logger;
    this.producer = kafka.producer();

    logger.debug(`producer created ${JSON.stringify(config)}`);
    this.config = config;
    this.retries = 0;
    this.init();
  }

  init() {
    if (this.retries > 3) {
      this.logger.error('CONNECTION RETRIES REACHED');
    }

    this.producer.connect().then(() => {
      this.logger.debug('CONECTION TO KAFKA SUCCESFUL');
      this.retries = 0;
    }).catch((error) => {
      this.logger.error('CONECTION TO KAFKA ERROR', error);

      this.retries += 1;
      this.init();
    });
  }

  async send(key, message, retries = 0) {
    this.logger.info(`sending ${JSON.stringify({ key, message })}`);
    if (retries >= 3) {
      this.logger.error('ERROR:', 'Imposible send data: ');
    }

    this.producer.send({
      topic: this.config.topic,
      messages: [{
        key: `${key}`,
        value: JSON.stringify(message),
      }],
    }).then(() => {
      this.logger.info(`OK sending data: ${JSON.stringify(message)}`);
    }).catch(async (error) => {
      if (error instanceof KafkaJSNumberOfRetriesExceeded) {
        await this.init();
      }
      this.logger.error('ERROR:', `sending data: ${JSON.stringify(message)}, error :${error}`);
      this.send(key, message, retries + 1);
    });
  }
}

KafkaClient.createProducer = (config, options = {}) => new Producer(config, options);
